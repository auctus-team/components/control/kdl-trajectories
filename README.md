# Continous integration
[![pipeline status](https://gitlab.inria.fr/auctus/panda/kdl_trajectories/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/kdl_trajectories)
[![Quality Gate Status](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=auctus%3Apanda%3Apanda-traj&metric=alert_status)](https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Apanda-traj)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=auctus%3Apanda%3Apanda-traj&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Apanda-traj)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Apanda-traj
- Documentation : https://auctus.gitlabpages.inria.fr/panda/kdl_trajectories/index.html


# Panda traj

Library using KDL to generate a trajectory
Can be used to adapt the trajectory online considering the maximum velocity of the robot.
The library is ros independent (except for the ros message types).

# Usage

**csv file**

Trajectories are described in a .csv file. Some examples are proposed in the trajectories folder.

`WAIT 1.0` Trajectory paused for one second.

`LOOP` Everything after the this input will be repeated indefinitely.

`MOVEL x y z r p y vel` Linear motion to the KDL::Frame(KDL::Rotation(`r`,`p`,`y`),KDL::Vector(`x`,`y`,`z`)) with a velocity of `vel`

`MOVEC x y z r p y r_end, p_end yaw_end px py pz vx vy vz alpha vel` Circular motion with ``r_end, p_end, yaw_end`` the orientation at the end of the path. ``px, py, pz`` the circle center position ``vx, vy, vz `` the circle axis vector. `alpha` ? and `vel` the maximum velocity along the path.

`GRIPPER HOMING` Gripper goes to home position.

`GRIPPER STOP` The gripper stops.

`GRIPPER MOVE width vel ` The gripper moves to reache a width of `width` with a velocity of `vel` m/s.

**Declare the trajectory generator:**

`TrajectoryGenerator trajectory;`

**Load a trajectory:**

`trajectory.Load("path/to/my_trajectory.csv);`

**Build the trajctory considering the current position X_curr:**

`trajectory.Build(KDL::Frame X_curr);`

A verbose can be added to print some information on the computed trajectory
The trajectory start from X_curr and goes to the first pose in the csv file

**Update the desired pose:**

`trajectory.updateTrajectory(traj_properties_, time_dt);`

**Get the desired pose along the trajectory** 

`KDL::Frame X_traj = trajectory.Pos();` The new desired pose.

`KDL::Twist Xd_traj = trajectory.Vel();` The new desired velocity.

`KDL::Twist Xdd_traj = trajectory.Acc();` The new desired Acceleration.

**Get the duration of the trajectory**

`double duration = trajectory.Duration();`

**Direction of motion**

`Eigen::Vector3d dir = trajectory.DirectionOfMotion(double time, double dt);`

Get the direction of motion given the current time on the trajectory and a small increment of time in the future.


**Publish the trajectory**

`trajectory.publishTrajectory();`

Populate a PublishTraj message consisting of a nav_msgs::Path 

### Traj_properties structure
Traj properties has the following structure:
```
    bool play_traj_
    bool jogging_
    bool gain_tunning_
    bool move_
    int64 index_
    float64 amplitude
    geometry_msgs/Pose X_curr_
    geometry_msgs/Pose X_des_jog_
```

If `play traj` is **True**, updateTrajectory will increase the time along the trajectory of time_dt seconds.

If `jogging_` is **True**, updateTrajectory will copy geometry_msgs/Pose X_des_jog_ to X_traj_.

If `gain_tunning_` is **True**, updateTrajectory will go in Gain Tunning mode.

If `gain_tunning_` is **True** and `move_` is **True**, the next Pose (X_traj) will do a step from the current pose (X_curr) to a new pose determined by the `index_` parameter by an amplitude of `amplitude`.

`index_` = 0 move along the x axis (`amplitude` in m).

`index_` = 1 move along the y axis (`amplitude` in m).

`index_` = 2 move along the z axis (`amplitude` in m).

`index_` = 3 rotate along the x axis (`amplitude` in rad).

`index_` = 4 rotate along the y axis (`amplitude` in rad).

`index_` = 5 rotate along the z axis (`amplitude` in rad).


### Note 

Some function are not document or no longer used 

`compute_time_vmax()`

`make_line_segment()`

`trajectory_progress()`
