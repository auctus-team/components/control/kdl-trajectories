/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TRAJECTORYGENERATOR_H
#define TRAJECTORYGENERATOR_H

#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chain.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/path_roundedcomposite.hpp>
#include <kdl/path_cyclic_closed.hpp>
#include <kdl/path_line.hpp>
#include <kdl/path_circle.hpp>
#include <kdl/trajectory_composite.hpp>
#include <kdl/trajectory_stationary.hpp>
#include <kdl/trajectory.hpp>
#include <kdl/trajectory_segment.hpp>
#include <kdl/rotational_interpolation_sa.hpp>
#include <kdl/utilities/error.h>
#include <kdl_conversions/kdl_msg.h>
#include <kdl/velocityprofile_trap.hpp>
#include <kdl/velocityprofile_spline.hpp>

#include <kdl_trajectories/PublishTraj.h>
#include <kdl_trajectories/TrajProperties.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <ros/node_handle.h>
#include <realtime_tools/realtime_publisher.h>
#include <kdl_trajectories/UpdateTrajectory.h>

#include <eigen_conversions/eigen_kdl.h>


#include <vector>
#include <string>
#include <Eigen/Dense>

#define NUM_CIRC_SEGS 8

struct MovementElement
{
  KDL::Frame frame;
  std::string movetype = "";
  std::vector<double> options;
};

struct LineSegment
{
  std::string movetype = "";
  KDL::Vector start_point;
  KDL::Vector end_point;
};

/**
 * Generates a Cartesian space trajectory with an underlying velocity profile
 */
class TrajectoryGenerator
{
private:
  KDL::Trajectory_Composite *ctraject;
  std::vector<MovementElement> movement_list;
  std::vector<double> movement_durations;
  std::vector<double> movement_vmax_old;
  double init_dur;
  double accmax = 1.0;
  Eigen::Vector3d u = Eigen::Vector3d(0, 1, 0);
  KDL::Frame task_frame, X_traj_, X_curr_, X_des_jog_;
  KDL::Twist Xd_traj_, Xdd_traj_;
  double t_traj_;
  bool play_traj_, gain_tunning_, move_;
  bool toggle_ = false;
  int index_;
  bool jogging_;

  /**
  * \brief
  * Compute an adjusted time value t match the pose of a default trajectory to another trajectory generated with maxvel_new and maxacc_new
  * \param Pos
  * \param Pos_length
  * \param duration_old
  * \param duration_new
  * \param maxvel_new
  * \param maxacc_new
  * \return time_dt
   */
  double compute_time_vmax(double Pos, double Pos_length, double duration_old, double duration_new, double maxvel_new,
                           double maxacc_new);

  /**
   * \brief
   * Create a line segment given the move type, start point, and end point.
   * \param movetype
   * \param v1
   * \param v2
   * \return line segment
   */
  LineSegment make_line_segment(std::string movetype, KDL::Vector v1, KDL::Vector v2);

  /**
   * \brief
   * Computes the closest point along the trajectory given the frame X_curr
   * \param X_curr
   */
  void trajectory_progress(KDL::Frame &X_curr);


public:
  std::vector<LineSegment> polyline;
  double percent_completion;
  
  bool in_loop = false; 
  // Wireloop variables
  std::vector<KDL::Vector> intersection_points;  
  KDL::Vector wireloop_point;

  /**
   * \brief
   * Load a csv file containing a desired velocity
   * \param X_curr The robot current pose
   */
  void Load(std::string &csv_file_name);

  /**
   * \brief
   * Builds a loaded Cartesian space trajectory.
   * \param X_curr The robot current pose
   * \param verbose optional. Display some information on the trajectory if True
   */
  void Build(KDL::Frame &X_curr, bool verbose);

  /**
   * \brief
   * Builds a loaded Cartesian space trajectory.
   * \param X_curr The robot current pose
   * \param verbose optional. Display some information on the trajectory if True
   */
  void Build(Eigen::Affine3d &X_curr, bool verbose);

  /**
   * \brief
   * Builds a loaded Cartesian space trajectory with a maximum velocity of vmax.
   * \param X_curr
   * \param vmax
   * \param amax
   */
  void Build_vmax(KDL::Frame &X_curr, double vmax, double amax);

  /**
   * \brief
   * Get the duration of the trajectory
   */
  double Duration();

  /**
   * \brief
   * Get the time until reaching the first point along the trajectory
   */
  double ResetDuration();

  /**
   * \brief
   * Set the maximum acceleration along the trajectory profile
   * \param amax The new maximal acceleration
   */
  void SetAccMax(double amax);

  /**
   * \brief
   * Get the desired Pose
   */
  Eigen::Affine3d Pos();

  /**
   * \brief
   * Get the desired Twist
   */
  Eigen::Matrix<double,6,1> Vel();

  /**
   * \brief
   * Get the desired Acceleration
   */
  Eigen::Matrix<double,6,1> Acc();

  /**
   * \brief
   * Get the current time along the trajectory
   */
  double CurrentTime();

  /**
   * \brief
   * Computes the direction of motion along the trajectory in dt at a given time
   * \param time The current time
   * \param dt The small time step to the next pose
   */
  Eigen::Vector3d DirectionOfMotion(double time, double dt);

  /**
   * \brief
   * Publish the trajectory
   */
  kdl_trajectories::PublishTraj publishTrajectory();

  /**
  * \brief
  * Updates the TrajProperties msg for the time time_dt_
  * \param traj_properties_
  * \param time_dt_
  */
  void updateTrajectory(kdl_trajectories::TrajProperties traj_properties_, double time_dt_);

  /**
  * \brief
  * Go back to the previous desired pose
  * \param traj_properties_
  * \param time_dt_
  */
  void previous(kdl_trajectories::TrajProperties traj_properties_, double time_dt_);

  bool traj_build_ = false;
};

#endif  // TRAJECTORYGENERATOR_H
